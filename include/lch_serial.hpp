/*
This file is part of LimonCore Home.

    LimonCore Home is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LimonCore Home is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LimonCore Home.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LCH_SERIAL_
#define _LCH_SERIAL_

#ifdef _WIN32
    #include <windows.h>
#else
    #include <errno.h>
    #include <sys/types.h>
    #include <fcntl.h>
#endif

#include <utils/lch_errors.hpp>

class LCH_SERIAL
{
    public:
        LCH_SERIAL();
        ~LCH_SERIAL();
    private:
        #ifdef _WIN32
            HANDLE portHandle;
        #else
            int portDesc = 0;
        #endif // _WIN32
};

#endif // _LCH_SERIAL_
