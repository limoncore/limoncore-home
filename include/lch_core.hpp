/*
This file is part of LimonCore Home.

    LimonCore Home is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LimonCore Home is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LimonCore Home.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LCH_CORE_
#define _LCH_CORE_

class LCH_CORE
{
    public:
        LCH_CORE();
        ~LCH_CORE();
    private:
        unsigned char version;
};

#endif // _LCH_CORE_
